import {FunctionComponent, ReactNode} from "react";

interface buttonParams {
    children: ReactNode,
    classList?: string,
    onclick?: any,
    type?: "button" | "reset" | "submit"
}

const Button: FunctionComponent<buttonParams> = (params: buttonParams) => {
    return (
        <button
            className={`button ${params.classList}`}
            onClick={params.onclick}
            type={params.type}>
            {params.children}
        </button>
    )
}

export default Button;