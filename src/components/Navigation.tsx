'use client';

import Link from "next/link";
import {usePathname} from "next/navigation";
import React, {FunctionComponent} from "react";

const Navigation: FunctionComponent = () => {
    const pathname = usePathname();

    return (
        <nav>
            <ul>
                <li className={`${pathname === '/' ? 'active' : ''}`}>
                    <Link href="/">Home</Link>
                </li>
                <li className={`${pathname === '/pokedex' ? 'active' : ''}`}>
                    <Link href="/pokedex">Pokedex</Link>
                </li>
                <li className={`${pathname === '/chucknorris' ? 'active' : ''}`}>
                    <Link href="/chucknorris">Chuck Norris</Link>
                </li>
            </ul>
        </nav>
    )
}

export default Navigation;