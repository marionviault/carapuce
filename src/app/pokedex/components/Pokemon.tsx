'use client';

import Image from "next/image";
import {GiBodyHeight} from "react-icons/gi";
import {FaWeightHanging} from "react-icons/fa";
import {pokemonParams} from "@/app/pokedex/interfaces/pokemonParams";
import {ReactElement} from "react";

export default function Pokemon(params: pokemonParams): ReactElement {

    return (
        <div className="pokemon">

            <h2>#{params.pokemonData.id} {params.pokemonData.name}</h2>
            <Image
                src={params.pokemonData.sprite} alt={params.pokemonData.name}
                width={400} height={400}
            />
            <audio controls autoPlay={true} key={params.pokemonData.id}>
                <source src={params.pokemonData.cry} type="audio/ogg"/>
            </audio>
            <div><GiBodyHeight/> {params.pokemonData.height} m</div>
            <div><FaWeightHanging/> {params.pokemonData.weight} kg</div>

        </div>
    );
}
