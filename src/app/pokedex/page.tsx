'use client';

import React, {ReactElement, useEffect, useState} from "react";
import "@/app/pokedex/styles/pokedex.css";
import Pokemon from "@/app/pokedex/components/Pokemon";
import usePokemonFetch from "@/app/pokedex/usePokemonFetch";
import Button from "@/components/Button";
import {TbPokeball} from "react-icons/tb";
import {FaSearch} from "react-icons/fa";
import Link from "next/link";
import {pokemonDataStructure} from "@/app/pokedex/interfaces/pokemonDataStructure";
import Image from "next/image";

export default function Pokedex(): ReactElement {

    const [pokemonNameOrId, setPokemonNameOrId] = useState<string>('pikachu');
    const [pokemonData, setPokemonData] = useState<pokemonDataStructure>({
        id: '',
        name: '',
        sprite: '',
        cry: '',
        height: 0,
        weight: 0
    });
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
        usePokemonFetch(pokemonNameOrId, setPokemonData).then((response: any): Promise<void> => response);
        event.preventDefault();
    }

    useEffect((): void => {
        usePokemonFetch(pokemonNameOrId, setPokemonData).then((response: any): Promise<void> => response);
    }, []);

    const displayPrevNextPokemon = (PokemonName: string): void => {
        usePokemonFetch(PokemonName, setPokemonData).then((response: any): Promise<void> => response);
    }

    return (

        <div className="pokedex">

            <h2>
                <TbPokeball/>
                <> </>
                <Link href="https://pokeapi.co/" target="_blank">PokeApi</Link>
            </h2>

            <h3>Pick a Pokemon</h3>

            <form onSubmit={handleSubmit}>
                <input type="text" value={pokemonNameOrId}
                       onChange={(event: React.ChangeEvent<HTMLInputElement>) => setPokemonNameOrId(event.target.value)}/>
                <Button classList='button-pokemon'>
                    <FaSearch/>
                </Button>
            </form>

            {pokemonData.name !== '' ?
                <Pokemon pokemonData={pokemonData}></Pokemon>
                : <div>Loading Pokemon</div>}

            {pokemonData.prevPokemon ?
                // todo
                // @ts-ignore
                <div onClick={() => displayPrevNextPokemon(pokemonData.prevPokemon.name)} class="pokemon-nav pokemon-prev">
                    {'< prev'} #{pokemonData.prevPokemon.id} {pokemonData.prevPokemon.name}
                    <> </>
                    <Image
                        src={pokemonData.prevPokemon.sprite}
                        width={40} height={40}
                        alt={pokemonData.prevPokemon.name}
                    />
                </div>
                : null}

            {pokemonData.nextPokemon ?
                // todo
                // @ts-ignore
                <div onClick={() => displayPrevNextPokemon(pokemonData.nextPokemon.name)} class="pokemon-nav pokemon-next">
                    #{pokemonData.nextPokemon.id} {pokemonData.nextPokemon.name}
                    <> </>
                    <Image
                        src={pokemonData.nextPokemon.sprite}
                        width={40} height={40}
                        alt={pokemonData.nextPokemon.name}
                    />
                    <> </>
                    {'next >'}
                </div>
                : null}

        </div>
    );
}
