import {pokemonRawDataStructure} from "@/app/pokedex/interfaces/pokemonRawDataStructure";
import {prevNextPokemonRawDataStructure} from "@/app/pokedex/interfaces/prevNextPokemonRawDataStructure";
import {pokemonDataStructure} from "@/app/pokedex/interfaces/pokemonDataStructure";
import React from "react";

const generateCleanData = (pokemonData: pokemonRawDataStructure, prevPokemonData: prevNextPokemonRawDataStructure | null, nextPokemonData: prevNextPokemonRawDataStructure | null): pokemonDataStructure => {
    const data: pokemonDataStructure = {
        id: String(pokemonData.id).padStart(3, '0'),
        name: pokemonData.name,
        sprite: pokemonData.sprites.other['official-artwork'].front_default,
        cry: pokemonData.cries.latest,
        height: pokemonData.height / 10,
        weight: pokemonData.weight / 10

    };
    if (prevPokemonData) {
        data.prevPokemon = {
            id: String(prevPokemonData.id).padStart(3, '0'),
            name: prevPokemonData.name,
            sprite: prevPokemonData.sprites.front_default
        }
    }
    if (nextPokemonData) {
        data.nextPokemon = {
            id: String(nextPokemonData.id).padStart(3, '0'),
            name: nextPokemonData.name,
            sprite: nextPokemonData.sprites.front_default
        }
    }
    return data;
}

const usePokemonFetch = async (pokemonNameOrId: string | number, setPokemonData: React.Dispatch<React.SetStateAction<pokemonDataStructure>>) => {

    const fetchPokemon = fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonNameOrId}`)
        .then(async response => {
            return response.ok ? await response.json() : null;
        })

    const pokemonData = await fetchPokemon;

    if (pokemonData) {

        const fetchPrevPokemon: Promise<prevNextPokemonRawDataStructure | null> = fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonData.id - 1}`)
            .then(async response => {
                return response.ok ? await response.json() : null;
            })

        const prevPokemonData: prevNextPokemonRawDataStructure | null = await fetchPrevPokemon;

        const fetchNextPokemon: Promise<prevNextPokemonRawDataStructure | null> = fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonData.id + 1}`)
            .then(async response => {
                return response.ok ? await response.json() : null;
            })

        const nextPokemonData: prevNextPokemonRawDataStructure | null = await fetchNextPokemon;


        setPokemonData(generateCleanData(pokemonData, prevPokemonData, nextPokemonData));

    }

}

export default usePokemonFetch;