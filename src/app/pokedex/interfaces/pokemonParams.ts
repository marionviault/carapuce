import {pokemonDataStructure} from "@/app/pokedex/interfaces/pokemonDataStructure";

export interface pokemonParams {
    pokemonData: pokemonDataStructure
}