export interface pokemonDataStructure {
    id: string,
    name: string,
    sprite: string,
    cry: string,
    height: number,
    weight: number,
    prevPokemon?: {
        id: string,
        name: string,
        sprite: string
    },
    nextPokemon?: {
        id: string,
        name: string,
        sprite: string
    }
}