export interface pokemonRawDataStructure {
    id: number,
    name: string,
    sprites: {
        other: {
            'official-artwork': {
                front_default: string
            }
        }
    },
    cries: {
        latest: string
    },
    height: number,
    weight: number
}