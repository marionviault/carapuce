import type {Metadata} from "next";
import "@/styles/styles.css";
import React from "react";
import Navigation from "@/components/Navigation";

export const metadata: Metadata = {
    title: "Carapuce",
    description: "My React project",
};

export default function RootLayout({children}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang="en">
        <body>
        <header>
            <h1>Carapuce</h1>
            <Navigation/>
        </header>
        {children}
        </body>
        </html>
    );
}
