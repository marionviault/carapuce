const useChuckNorrisFetch = async (setChuckNorrisJoke: any) => {
    fetch(`https://api.chucknorris.io/jokes/random`)
        .then((response: Response) => response.json())
        .then((data) => {
            setChuckNorrisJoke(data.value);
        })
        .catch((error) => console.log(error));
}

export default useChuckNorrisFetch;