'use client';

import React, {useEffect, useState} from "react";
import useChuckNorrisFetch from "@/app/chucknorris/useChuckNorrisFetch";
import Button from "@/components/Button";
import Link from "next/link";

export default function ChuckNorris() {

    const [chuckNorrisJoke, setChuckNorrisJoke] = useState('');

    useEffect(() => {
        useChuckNorrisFetch(setChuckNorrisJoke).then(r => r);
    }, []);

    return (

        <div>

            <h2>
                <Link href="https://api.chucknorris.io/" target="_blank">Chuck Norris</Link>
            </h2>

            <Button onclick={() => useChuckNorrisFetch(setChuckNorrisJoke)} classList='button-chucknorris'>
                Get a random joke
            </Button>

            <div>{chuckNorrisJoke}</div>

        </div>
    );
}
